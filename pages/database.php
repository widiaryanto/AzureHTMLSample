<!DOCTYPE html>
<html lang="en">
<head>
  <title>Microsoft Azure Cloud</title>
  <link rel="stylesheet" href="css/materialize.min.css">  
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
  <!-- Navigasi -->
  <nav class="teal lighten-1" role="navigation">
    <div class="nav-wrapper container">
      <a href="/index.html" class="brand-logo" id="logo-container">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Databases Register</a>
      <i class="material-icons">arrow_back</i>
    </div>
  </nav>
  <!-- Akhir Navigasi -->
  <div class="body-content container">
  <?php
    $host = "widiaryantowebappserver.database.windows.net";
    $user = "widiaryanto";
    $pass = "DJpw&24680!";
    $db = "widiaryantowebapp";
    try {
      $conn = new PDO("sqlsrv:server = $host; Database = $db", $user, $pass);
      $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    } catch(Exception $e) {
      echo "Failed: " . $e;
    }
    if (isset($_POST['submit'])) {
      try {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $job = $_POST['job'];
        $date = date("Y-m-d");
        // Insert data
        $sql_insert = "INSERT INTO Registration (name, email, job, date) VALUES (?,?,?,?)";
        $stmt = $conn->prepare($sql_insert);
        $stmt->bindValue(1, $name);
        $stmt->bindValue(2, $email);
        $stmt->bindValue(3, $job);
        $stmt->bindValue(4, $date);
        $stmt->execute();
      } catch(Exception $e) {
        echo "Failed: " . $e;
      }
      echo "<h4>Your're registered!</h4>";
      try {
        $sql_select = "SELECT * FROM Registration";
        $stmt = $conn->query($sql_select);
        $registrants = $stmt->fetchAll();
        if(count($registrants) > 0) {
          echo "<table class='responsive-table'>";
          echo "<tr><th>Name</th>";
          echo "<th>Email</th>";
          echo "<th>Job</th>";
          echo "<th>Date</th></tr>";
          foreach($registrants as $registrant) {
            echo "<tr><td>".$registrant['name']."</td>";
            echo "<td>".$registrant['email']."</td>";
            echo "<td>".$registrant['job']."</td>";
            echo "<td>".$registrant['date']."</td></tr>";
          }
          echo "</table>";
        } else {
          echo "<h4>No one is currently registered.</h4>";
        }
      } catch(Exception $e) {
        echo "Failed: " . $e;
      }
    } else if (isset($_POST['load_data'])) {
      try {
        $sql_select = "SELECT * FROM Registration";
        $stmt = $conn->query($sql_select);
        $registrants = $stmt->fetchAll();
        if(count($registrants) > 0) {
          echo "<h4>People who are registered:</h4>";
          echo "<table class='responsive-table'>";
          echo "<tr><th>Name</th>";
          echo "<th>Email</th>";
          echo "<th>Job</th>";
          echo "<th>Date</th></tr>";
          foreach($registrants as $registrant) {
            echo "<tr><td>".$registrant['name']."</td>";
            echo "<td>".$registrant['email']."</td>";
            echo "<td>".$registrant['job']."</td>";
            echo "<td>".$registrant['date']."</td></tr>";
          }
          echo "</table>";
        } else {
          echo "<h4>No one is currently registered.</h4>";
        }
      } catch(Exception $e) {
        echo "Failed: " . $e;
      }
    }
  ?>
  <div align="center">
    <p>Copyright <strong>© 2019 Muhamad Widi Aryanto</strong></p>
  </div>
</div>
<script src="js/materialize.min.js"></script>
</body>
</html>