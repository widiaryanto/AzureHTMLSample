<!DOCTYPE html>
<html lang="en">
<head>
  <title>Microsoft Azure Cloud</title>
  <link rel="stylesheet" href="css/materialize.min.css">  
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script> 
</head>
<body>
  <!-- Navigasi -->
  <nav class="teal lighten-1" role="navigation">
    <div class="nav-wrapper container">
      <a href="/index.html" class="brand-logo" id="logo-container">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Analyze Image</a>
      <i class="material-icons">arrow_back</i>
    </div>
  </nav>
  <!-- Akhir Navigasi -->
  <div class="body-content container">
  <script type="text/javascript">
    function processImage() {
        // **********************************************
        // *** Update or verify the following values. ***
        // **********************************************
 
        // Replace <Subscription Key> with your valid subscription key.
        var subscriptionKey = "7c3e2550e0da4dae83bca9d2002dd657";
 
        // You must use the same Azure region in your REST API method as you used to
        // get your subscription keys. For example, if you got your subscription keys
        // from the West US region, replace "westcentralus" in the URL
        // below with "westus".
        //
        // Free trial subscription keys are generated in the "westus" region.
        // If you use a free trial subscription key, you shouldn't need to change
        // this region.
        var uriBase ="https://southeastasia.api.cognitive.microsoft.com/vision/v2.0/analyze";
 
        // Request parameters.
        var params = {
            "visualFeatures": "Categories,Description,Color",
            "details": "",
            "language": "en",
        };
 
        // Display the image.
        var sourceImageUrl = document.getElementById("inputImage").value;
        document.querySelector("#sourceImage").src = sourceImageUrl;
 
        // Make the REST API call.
        $.ajax({
            url: uriBase + "?" + $.param(params),
            // Request headers.
            beforeSend: function(xhrObj){
                xhrObj.setRequestHeader("Content-Type","application/json");
                xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key", subscriptionKey);
            },
            type: "POST",
            // Request body.
            data: '{"url": ' + '"' + sourceImageUrl + '"}',
        }).done(function(data) {
            // Show formatted JSON on webpage.
            jsonData = JSON.stringify(data, null, 2);
            object = JSON.parse(jsonData);
            output = document.getElementById('responseTextArea');
            output.innerHTML = object.description.captions[0].text + ' ';
        }).fail(function(jqXHR, textStatus, errorThrown) {
            // Display error message.
            var errorString = (errorThrown === "") ? "Error. " :
                errorThrown + " (" + jqXHR.status + "): ";
            errorString += (jqXHR.responseText === "") ? "" :
                jQuery.parseJSON(jqXHR.responseText).message;
            alert(errorString);
        });
    };
  </script>
  <p>Enter the URL to an image, then click the <strong>Analyze image</strong> button.</p>
  <input type="text" name="inputImage" id="inputImage"/>
  <button class="btn-small blue" onclick="processImage()">Analyze image</button>
  <br><br>
  <div id="wrapper" style="width:1024px; display:table;">
      <div id="imageDiv" style="width:500px; display:table-cell;">
          Preview :
          <br><br>
          <img id="sourceImage" width="500" />
      </div>
      <div id="jsonOutput" style="width:424px; display:table-cell;">
          Result :
          <p id="responseTextArea" style="width:400px; height:50px;"></p>
      </div>
  </div>
  <div align="center">
    <p>Copyright <strong>© 2019 Muhamad Widi Aryanto</strong></p>
  </div>
</div>
  <script src="js/materialize.min.js"></script>
</body>
</html>