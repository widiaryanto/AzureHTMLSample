<div class="body-content">
  <p>Fill in your name and email address, then click <strong>Submit</strong> to register.</p>
  <form method="post" action="pages/database.php" enctype="multipart/form-data" >
    Name  <input type="text" placeholder="Type your name here" name="name" id="name"/></br></br>
    Email <input type="text" placeholder="Type your valid email here" name="email" id="email"/></br></br>
    Job <input type="text" placeholder="Type your job now here" name="job" id="job"/></br></br>
    <input type="submit" class="btn-small blue" name="submit" value="Submit" />
    <input type="submit" class="btn-small blue" name="load_data" value="Load Data" />
  </form>
  <?php
    $host = "widiaryantowebappserver.database.windows.net";
    $user = "widiaryanto";
    $pass = "DJpw&24680!";
    $db = "widiaryantowebapp";
    try {
      $conn = new PDO("sqlsrv:server = $host; Database = $db", $user, $pass);
      $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    } catch(Exception $e) {
      echo "Failed: " . $e;
    }
    if (isset($_POST['submit'])) {
      try {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $job = $_POST['job'];
        $date = date("Y-m-d");
        // Insert data
        $sql_insert = "INSERT INTO Registration (name, email, job, date) VALUES (?,?,?,?)";
        $stmt = $conn->prepare($sql_insert);
        $stmt->bindValue(1, $name);
        $stmt->bindValue(2, $email);
        $stmt->bindValue(3, $job);
        $stmt->bindValue(4, $date);
        $stmt->execute();
      } catch(Exception $e) {
        echo "Failed: " . $e;
      }
      echo "<h4>Your're registered!</h4>";
    } else if (isset($_POST['load_data'])) {
      try {
        $sql_select = "SELECT * FROM Registration";
        $stmt = $conn->query($sql_select);
        $registrants = $stmt->fetchAll();
        if(count($registrants) > 0) {
          echo "<h4>People who are registered:</h4>";
          echo "<table class='responsive-table'>";
          echo "<tr><th>Name</th>";
          echo "<th>Email</th>";
          echo "<th>Job</th>";
          echo "<th>Date</th></tr>";
          foreach($registrants as $registrant) {
            echo "<tr><td>".$registrant['name']."</td>";
            echo "<td>".$registrant['email']."</td>";
            echo "<td>".$registrant['job']."</td>";
            echo "<td>".$registrant['date']."</td></tr>";
          }
          echo "</table>";
        } else {
          echo "<h4>No one is currently registered.</h4>";
        }
      } catch(Exception $e) {
        echo "Failed: " . $e;
      }
    }
  ?>
  <div align="center">
    <p>Copyright <strong>© 2019 Muhamad Widi Aryanto</strong></p>
  </div>
</div>